#include <iostream>
#include <string>
using namespace std;

int main()
{
    int age, perm, acc, assur, p, a(0);
    bool C1, C2, C3;
    string situ;

    cout << "Si vous voulez tester l'assurance d'un candidat, tapez 1, pour quitter, tapez -1" << endl;
    cin >> a;

    while (a == 1)
    {
        cout << "Entrez l'age" << endl;
        cin >> age;
        cout << "Entrez le nombre d'annees de permis : " << endl;
        cin >> perm;
        cout << "Entrez le nombre d'accidents : " << endl;
        cin >> acc;
        cout << "Entrez le nombre d'annees d'assurance :" << endl;
        cin >> assur;

        C1 = age >= 25;
        C2 = perm >= 2;
        C3 = assur > 5;
        p = 0;

        if (C1 == false)
        {
            p = p + 1;
        }
        if (C2 == false)
        {
            p = p + 1;
        }

        p = p + acc;

        if (p < 3 && C3)
        {
            p = p - 1;
        }
        if (p == -1)
        {
            situ = "Bleu";
        }
        else if (p == 0)
        {
            situ = "Vert";
        }
        else if (p == 1)
        {
            situ = "Orange";
        }
        else if (p == 2)
        {
            situ = "Rouge";
        }
        else
        {
            situ = "Refuse";
        }

        cout << "Votre situation : " << situ << endl;

        cout << "Si vous voulez tester l'assurance d'un candidat, tapez 1, pour quitter, tapez -1" << endl;
        cin >> a;
    }
}